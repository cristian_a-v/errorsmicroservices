#include <iostream>
#include <HelloOperation.grpc.pb.h>
#include <grpc/grpc.h>
#include <grpcpp/channel.h>
#include <grpcpp/client_context.h>
#include <grpcpp/create_channel.h>
#include <grpcpp/security/credentials.h>
using grpc::Channel;
using grpc::ClientContext;
using grpc::ClientReader;
using grpc::ClientReaderWriter;
using grpc::ClientWriter;

int main()
{
	grpc_init();
	ClientContext context;
	auto sum_stub = SayHelloService::NewStub(grpc::CreateChannel("localhost:8888",
		grpc::InsecureChannelCredentials()));
	NameRequest nameRequest;
	std::string name;
	name = "Alexandru99";
	nameRequest.set_name(name);
	OperationResponse response;
	auto status = sum_stub->Salute(&context, nameRequest, &response);
	if (status.ok()) {
		std::cout << "Succesed\n\n";
	}

	ClientContext c2;
	status = sum_stub->SaluteValidNames(&c2, nameRequest, &response);
	//Nu va reusi deoarece numele nostru contine cifre
	if (status.ok()) {
		std::cout << "Succesed\n";
	}
	else {
		//in caz ca nu s-a reusit afisam mesajul erorii
		std::cout << status.error_message()<<"\n";

		//sau putem trata o anumita exceptie individual
		if (status.error_code() == grpc::StatusCode::INVALID_ARGUMENT) {
			//
			std::cout << "S-a detectat INVALID_ARGUMENT error\n";
		}
	}
	ClientContext c3;
	status = sum_stub->SaluteUnimplemented(&c3, nameRequest, &response);
	std::cout <<"\n"<< status.error_message();
}

