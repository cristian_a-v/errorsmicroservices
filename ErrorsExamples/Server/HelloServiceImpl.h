#pragma once
#include <HelloOperation.grpc.pb.h>

class HelloServiceImpl final : public SayHelloService::Service
{
public:
	HelloServiceImpl() {};
	::grpc::Status Salute(::grpc::ServerContext* context, const ::NameRequest* request, ::OperationResponse* response) override;
	::grpc::Status SaluteValidNames(::grpc::ServerContext* context, const ::NameRequest* request, ::OperationResponse* response) override;
	::grpc::Status SaluteUnimplemented(::grpc::ServerContext* context, const ::NameRequest* request, ::OperationResponse* response) override;
};

