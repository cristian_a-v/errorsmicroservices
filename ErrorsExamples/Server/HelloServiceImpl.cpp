#include "HelloServiceImpl.h"

::grpc::Status HelloServiceImpl::Salute(::grpc::ServerContext* context, const::NameRequest* request, ::OperationResponse* response)
{
	std::string message = request->name();
	std::cout << message<<"\n";
	return ::grpc::Status::OK;
}

::grpc::Status HelloServiceImpl::SaluteValidNames(::grpc::ServerContext* context, const::NameRequest* request, ::OperationResponse* response)
{
	std::string message = request->name();
	//if (std::any_of(message.begin(), message.end(), ::isdigit))
	//{
		return ::grpc::Status(grpc::StatusCode::INVALID_ARGUMENT, "Un nume cu cifre nu este valid");
	//}
	std::cout << message << "\n";
	return ::grpc::Status::OK;
}

::grpc::Status HelloServiceImpl::SaluteUnimplemented(::grpc::ServerContext* context, const::NameRequest* request, ::OperationResponse* response)
{
	return ::grpc::Status(grpc::StatusCode::UNIMPLEMENTED, "Metoda nu e implementata");
}
